# README #

Este documento README pretende recoger la información general de este repositorio, donde estará alojado un proyecto de Spring boot para el curso de Java on Cloud impartido por Core Networks Sevilla.

### Conocimientos adquiridos ###

* Java SE 8
* Java EE 8
	* Streams
	* Filtros
	* Expresiones lambda
	* JDBC con MySQL
	* Spring
	* Hibernate
	* Y un largo etcétera

### ¿Para qué sirve este repositorio? ###

* Para mostrar mis avances con Spring
* Llevar un control de versiones

### ¿Dónde estoy? ###

* [Twitter](https://twitter.com/lk2_89)
* [Facebook](https://www.facebook.com/pablojosedelgadoflores)
* [Linkedin](https://www.linkedin.com/in/pablojosedelgadoflores)
* Si deseas conocer mis otros proyectos en la red, entra en [TodoBytes](https://todobytes.es) y en [Sudosu](http://sudosu.es)