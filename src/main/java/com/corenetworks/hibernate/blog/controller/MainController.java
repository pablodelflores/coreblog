package com.corenetworks.hibernate.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.blog.dao.UserDao;

import com.corenetworks.hibernate.blog.model.User;

@Controller
public class MainController {
	
	@Autowired
	private UserDao usuarioDao;
	
	@GetMapping(value="/")
	public String welcome(Model modelo) {
		List<User> usuarios = usuarioDao.getAll();
		modelo.addAttribute("usuariosLista", usuarios);
		return "index";
	}

}
