package com.corenetworks.hibernate.blog.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

@Entity
public class User {
	@Id
	//Con generatedvalue hacemos el incremento automatico del id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="NAME")
	private String nombre;
	
	@Column
	private String ciudad;
	
	@Column
	private String email;
	
	@Column
	@ColumnTransformer(write=" MD5(?) ")
	private String password;
	
	@Column
	//Ahora le decimos que coja la hora del sistema
	@CreationTimestamp
	private Date fechaAlta;
	
	public User() {
		
	}
	
	public User(String nombre, String ciudad, String email, String password) {
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.email = email;
		this.password = password;
	}
	
	//GETTERS y SETTERS
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nombre=" + nombre + ", ciudad=" + ciudad + ", email=" + email + ", password="
				+ password + ", fechaAlta=" + fechaAlta + "]";
	}
	
}
