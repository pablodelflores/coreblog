package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.corenetworks.hibernate.blog.model.User;

@Repository
@Transactional
public class UserDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	/*
	 * Almacena el isuario en la BBDD
	 */
	
	public void create(User usuario) {
		entityManager.persist(usuario);
		return;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAll() {
		return entityManager
				.createQuery("select u from User u")
				.getResultList();
	}
}